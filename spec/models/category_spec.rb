require 'rails_helper'

RSpec.describe Category, type: :model do
  subject { create :category }

  describe 'validations' do
    context 'name' do
      it { should validate_presence_of(:name) }
    end
  end

  describe 'associations' do
    it { should have_and_belong_to_many(:events) }
  end
end
