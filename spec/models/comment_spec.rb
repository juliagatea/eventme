require 'rails_helper'

RSpec.describe Comment, type: :model do
  subject { create :comment }

  describe 'validations' do
    context 'description' do
      it { should validate_presence_of(:description) }

      it { should validate_length_of(:description).is_at_least(10) }
    end
  end

  describe 'associations' do
    it { should belong_to(:event) }

    it { should belong_to(:user) }
  end
end
