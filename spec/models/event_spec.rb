require 'rails_helper'

RSpec.describe Event, type: :model do
  subject { create :event }

  describe 'validations' do
    context 'name' do
      it { should validate_presence_of(:name) }

      it { should validate_length_of(:name).is_at_least(3).is_at_most(25) }
    end

    context 'description' do
      it { should validate_presence_of(:description) }

      it { should validate_length_of(:description).is_at_least(15) }
    end

    context 'date' do
      it { should validate_presence_of(:date) }
    end

    context 'location' do
      it { should validate_presence_of(:location) }
    end

    it { is_expected.to be_valid }
  end

  describe 'associations' do
    it { should belong_to(:user) }

    it { should have_and_belong_to_many(:subscribers) }

    it { should have_and_belong_to_many(:categories) }

    it { should have_many(:comments) }
  end
end
