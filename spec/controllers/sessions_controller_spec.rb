require 'rails_helper'

RSpec.describe SessionsController, type: :controller do

  describe 'GET #new' do
    subject { get :new }

    it 'should have http status success' do
      expect(subject).to have_http_status(200)
    end

    let(:user) { create(:user, email: 'test@test.com', password: 'P@ssw0rd') }
    let(:log_in) do
      post(:create, params: { session: { email: user.email,
                                         password: 'P@ssw0rd' } })
    end
    it 'should redirect if logged in' do
      log_in
      expect(response).to redirect_to root_path
    end
  end

  describe('POST #create') do
    let(:user) { create(:user, email: 'test@test.com', password: 'P@ssw0rd') }

    context 'with valid credentials' do
      subject do
        post :create, params: { session: { email: user.email,
                                           password: 'P@ssw0rd' } }
      end

      it 'should log in' do
        subject
        expect(session[:user_id]).to eq(user.id)
      end

      it 'should redirect to root path' do
        subject
        expect(subject).to redirect_to root_path
      end
    end

    context 'with invalid credentials' do
      subject do
        post :create, params: { session: { email: user.email,
                                           password: 'parola' } }
      end

      it 'should not log in' do
        subject
        expect(session[:user_id]).to be(nil)
      end
    end
  end

  describe('DELETE #destroy') do
    let(:user) { create(:user, email: 'test@test.com', password: 'P@ssw0rd') }

    subject do
      post :destroy, params: { session: { user_id: user.id } }
    end

    before(:each) do
      login_as(user)
      subject
    end

    it 'should logout logged in user' do
      expect(session[:user_id]).to be(nil)
    end

    it 'should redirect to root path' do
      expect(response).to redirect_to root_path
    end
  end

end
