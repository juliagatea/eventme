require 'rails_helper'

RSpec.describe RegistrationsController, type: :controller do

  describe 'GET #new' do
    subject { get :new }

    it 'should have http status success' do
      expect(subject).to have_http_status(200)
    end

    it 'should call new on User' do
      expect(User).to receive(:new).and_call_original
      subject
    end

    let(:user) { create(:user, email: 'test@test.com', password: 'P@ssw0rd') }
    it 'should redirect to root path if logged in' do
      login_as(user)
      subject
      expect(subject).to redirect_to root_path
    end
  end

  describe('POST #create') do
    context 'with valid input' do
      subject do
        post :create, params: { user: { first_name: 'Test',
                                        last_name: 'Test',
                                        email: 'test@test.com',
                                        password: 'P@ssw0rd' } }
      end

      it 'should create user' do
        expect { subject }.to change(User, :count).by(1)
      end

      it 'should log in after create' do
        subject
        expect(session[:user_id]).to eq(User.last.id)
      end

      it 'should redirect to root path' do
        subject
        expect(subject).to redirect_to root_path
      end
    end

    context 'with invalid input' do
      subject do
        post :create, params: { user: { first_name: 'Test',
                                        last_name: 'Test',
                                        email: 'test@test.com',
                                        password: 'parola' } }
      end

      it 'should not save user' do
        subject
        expect { subject }.to_not change(User, :count)
      end

      it 'should not login' do
        subject
        expect(session[:user_id]).to be(nil)
      end
    end
  end
end
