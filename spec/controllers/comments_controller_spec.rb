require 'rails_helper'

RSpec.describe CommentsController, type: :controller do

  describe 'POST #create' do
    let(:user) { create(:user) }
    let(:event) { create(:event, user: user) }

    context 'with logged in user' do

      context 'owner of the event' do
        before(:each) do
          login_as(user)
        end

        context 'with valid input' do
          subject do
            post :create, params: { comment: attributes_for(:comment, user: user), event_id: event.id }
          end

          it 'should create comment' do
            expect { subject }.to change(Comment, :count).by(1)
          end

          it 'should redirect to event path' do
            expect(subject).to redirect_to event_path(event.id)
          end
        end

        context 'with invalid input' do
          subject do
            post :create, params: { comment: attributes_for(:comment, :invalid), event_id: event.id }
          end
          it 'should not create comment' do
            expect { subject }.to_not change(Comment, :count)
          end
        end
      end

      context 'not owner of the event' do
        let(:user1) { create(:user) }

        before(:each) do
          login_as(user1)
        end

        context 'with valid input' do
          subject do
            post :create, params: { comment: attributes_for(:comment, user: user1), event_id: event.id }
          end

          it 'should create comment' do
            expect { subject }.to change(Comment, :count).by(1)
          end

          it 'should redirect to event path' do
            expect(subject).to redirect_to event_path(event.id)
          end
        end

        context 'with invalid input' do
          subject do
            post :create, params: { comment: attributes_for(:comment, :invalid), event_id: event.id }
          end
          it 'should not create comment' do
            expect { subject }.to_not change(Comment, :count)
          end
        end
      end
    end

    context 'with not logged in user' do
      it 'should not create comment' do
        expect { subject }.to_not change(Comment, :count)
      end
    end
  end

  describe 'PATCH #update' do
    let(:user) { create(:user) }
    let(:event) { create(:event, user: user) }
    let(:comment) { create(:comment, event_id: event.id, user: user) }

    context 'existing comment' do
      context 'with logged in user' do

        context 'owner of the comment' do
          before(:each) do
            login_as(user)
          end

          context 'with valid input' do
            subject do
              patch :update, params: { id: comment.id,
                                       comment: attributes_for(:comment, description: 'I updated this comment.'),
                                       event_id: event.id }
            end

            it 'should change the comment attrbutes' do
              subject
              comment.reload
              expect(comment.description).to eq('I updated this comment.')
            end

            it 'should redirect to event' do
              expect(subject).to redirect_to event_path(event.id)
            end
          end

          context 'with invalid input' do
            subject do
              patch :update, params: { id: comment.id,
                                       comment: attributes_for(:comment, description: 'upd'),
                                       event_id: event.id }
            end

            it 'should not change the comment attrbutes' do
              subject
              comment.reload
              expect(comment.description).to_not eq('upd')
            end
          end
        end

        context 'not owner of the comment' do
          let(:user1) { create(:user) }
          before(:each) do
            login_as(user1)
          end
          subject do
            patch :update, params: { id: comment.id,
                                     comment: attributes_for(:comment, description: 'I updated this comment.'),
                                     event_id: event.id }
          end

          it 'should not change the comment attrbutes' do
            subject
            comment.reload
            expect(comment.description).to_not eq('I updated this comment.')
          end
        end
      end

      context 'with not logged in user' do
        subject do
          patch :update, params: { id: comment.id,
                                   comment: attributes_for(:comment, description: 'I updated this comment.'),
                                   event_id: event.id }
        end
        it 'should not change the comment attributes' do
          subject
          comment.reload
          expect(comment.description).to_not eq('I updated this comment.')
        end
      end
    end
    context 'non existing comment' do
      let(:bad_comment_id) { '20w' }
      subject { delete :destroy, params: { id: bad_comment_id, event_id: event.id } }

      it 'should respond with a 404' do
        login_as(user)
        expect(subject).to have_http_status(404)
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:user) { create(:user) }
    let(:event) { create(:event, user: user) }
    let!(:comment) { create(:comment, event_id: event.id, user: user) }

    context 'existing comment' do
      subject { delete :destroy, params: { id: comment.id, event_id: event.id } }

      context 'with logged in user' do

        context 'owner of the comment' do
          before(:each) do
            login_as(user)
          end

          it 'should delete comment' do
            expect { subject }.to change(Comment, :count).by(-1)
          end

          it 'should redirect to event' do
            expect(subject).to redirect_to event_path(event.id)
          end
        end

        context 'not owner of the comment' do
          let(:user1) { create(:user) }
          before(:each) do
            login_as(user1)
          end

          it 'should not delete comment' do
            expect { subject }.to_not change(Comment, :count)
          end
        end
      end

      context 'with not logged in user' do
        it 'should not delete comment' do
          expect { subject }.to_not change(Comment, :count)
        end
      end
    end
    context 'non existing comment' do
      let(:bad_comment_id) { '20w' }
      subject { delete :destroy, params: { id: bad_comment_id, event_id: event.id } }

      it 'should respond with a 404' do
        login_as(user)
        expect(subject).to have_http_status(404)
      end
    end
  end
end
