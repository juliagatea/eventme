require 'rails_helper'

RSpec.describe SubscribersController, type: :controller do

  context 'POST #create' do
    let(:owner) { create(:user) }
    let(:event) { create(:event, user: owner) }

    context 'existing event' do
      subject { post :create, params: { event_id: event.id } }

      context 'logged in user' do
        context 'owner of the event' do
          before(:each) do
            login_as(owner)
          end

          it 'should not add user as subscriber' do
            expect { subject }.to_not change(event.subscribers, :count)
          end
        end
        context 'not owner of the event' do
          let(:event_with_subscribers) { create(:event, :with_subscribers, user: owner) }
          let(:subscriber) { event_with_subscribers.subscribers.first }
          let(:new_subscriber) { create(:user, email: 'test1@test.com') }

          context 'already subscribed' do
            before(:each) do
              login_as(subscriber)
            end

            subject { post :create, params: { event_id: event_with_subscribers.id } }

            it 'should not add user as subscriber' do
              expect { subject }.to_not change(event_with_subscribers.subscribers, :count)
            end
          end

          context 'not subscribed to the event' do
            before(:each) do
              login_as(new_subscriber)
            end

            it 'should add user as subscriber' do
              expect { subject }.to change(event.subscribers, :count).by(1)
            end
          end
        end
      end
      context 'not logged in user' do
        it 'should not add user as subscriber' do
          expect { subject }.to_not change(event.subscribers, :count)
        end
      end
    end

    context 'non existing events' do
      let(:bad_event_id) { '20w' }
      subject { post :create, params: { event_id: bad_event_id } }
      before(:each) do
        login_as(owner)
      end

      it 'should respond with a 404' do
        expect(subject).to have_http_status(404)
      end
    end
  end

  context 'DELETE #destroy' do
    let(:owner) { create(:user) }

    context 'existing event' do
      context 'already subscribed to the event' do
        let(:event) { create(:event, :with_subscribers, user: owner) }
        let(:subscriber) { event.subscribers.first }

        subject { delete :destroy, params: { event_id: event.id } }

        context 'logged in user' do
          context 'owner of the event' do
            before(:each) do
              login_as(owner)
            end

            it 'should not be able to unsubscribe' do
              expect { subject }.to_not change(event.subscribers, :count)
            end
          end

          context 'not owner of the event' do
            before(:each) do
              login_as(subscriber)
            end

            it 'should unsubscribe' do
              expect { subject }.to change(event.subscribers, :count).by(-1)
            end
          end
        end

        context 'not logged in user' do
          it 'should not be able to unsubscribe' do

            expect { subject }.to_not change(event.subscribers, :count)
          end
        end
      end

      context 'not subscribed to the event' do
        let(:event) { create(:event, user: owner) }

        it 'should not be able to unsubscribe' do
          expect { subject }.to_not change(event.subscribers, :count)
        end
      end
    end

    context 'non existing events' do
      let(:subscriber) { create(:user) }
      let(:bad_event_id) { '20w' }

      before(:each) do
        login_as(subscriber)
      end

      subject { delete :destroy, params: { event_id: bad_event_id} }

      it 'should respond with a 404' do
        expect(subject).to have_http_status(404)
      end
    end
  end
end
