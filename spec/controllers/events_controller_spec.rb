require 'rails_helper'

RSpec.describe EventsController, type: :controller do

  describe 'GET #index' do
    subject { get :index }
    let(:event1) { create(:event) }
    let(:event2) { create(:event, date: '2018-12-12') }

    it 'populates an array of ordered Events' do
      expect(Event.order(:date)).to eq([event2, event1])
      subject
    end

    it { should have_http_status(200) }
  end

  describe 'GET #new' do
    subject { get :new }

    context 'with logged in user' do
      let(:user) { create(:user, email: 'test@test.com', password: 'P@ssw0rd') }

      before(:each) do
        login_as(user)
      end

      it 'should have http status success' do
        subject
        expect(subject).to have_http_status(200)
      end

      it 'should call new on Event' do
        expect(Event).to receive(:new).and_call_original
        subject
      end
    end

    context 'with not logged in user' do
      it 'should redirect to root path if not logged in' do
        subject
        expect(subject).to redirect_to root_path
      end
    end
  end

  describe 'POST #create' do
    context 'with logged in user' do
      let(:user) { create(:user, email: 'test@test.com', password: 'P@ssw0rd') }
      before(:each) do
        login_as(user)
      end

      context 'with valid input' do
        subject do
          post :create, params: { event: attributes_for(:event) }
        end

        it 'should create event' do
          expect { subject }.to change(Event, :count).by(1)
        end

        it 'should redirect to created Event path' do
          subject
          expect(subject).to redirect_to event_path(Event.last)
        end
      end

      context 'with invalid input' do
        subject do
          post :create, params: { event: attributes_for(:event, :invalid) }
        end

        it 'should not create event' do
          subject
          expect { subject }.to_not change(Event, :count)
        end
      end
    end
    context 'with not logged in user' do
      subject do
        post :create, params: { event: attributes_for(:event) }
      end

      it 'should not create event' do
        subject
        expect { subject }.to_not change(Event, :count)
      end
    end
  end

  describe 'GET #show' do
    let(:event) { create(:event) }

    context 'existing event' do
      subject { get :show, params: { id: event.id } }

      it 'should have http status success' do
        expect(subject).to have_http_status(200)
      end

      it 'assigns the requested event to @event' do
        expect(Event.find(event.id)).to eq(event)
      end
    end

    context 'non existing event' do
      let(:bad_event_id) { '20w' }
      subject { get :show, params: { id: bad_event_id } }

      it 'should raise exception' do
        expect { Event.find(bad_event_id) }.to raise_exception(ActiveRecord::RecordNotFound)
      end

      it 'should respond with a 404' do
        expect(subject).to have_http_status(404)
      end
    end
  end

  describe 'GET #edit' do
    let(:user) { create(:user, email: 'test@test.com', password: 'P@ssw0rd') }
    let(:event) { create(:event, user: user) }
    context 'existing event' do
      subject { get :edit, params: { id: event.id } }

      context 'with logged in user' do
        context 'owner of event' do
          before(:each) do
            login_as(user)
          end

          it 'should have http status success' do
            expect(subject).to have_http_status(200)
          end

          it 'assigns the requested event to @event' do
            expect(Event.find(event.id)).to eq(event)
          end
        end

        context 'not owner of event' do
          let(:user1) { create(:user, email: 'test1@test.com', password: 'P@ssw0rd') }
          before(:each) do
            login_as(user1)
          end

          it 'should redirect to event path' do
            expect(subject).to redirect_to root_path
          end
        end
      end
      context 'with not logged in user' do
        it 'should redirect to root path' do
          expect(subject).to redirect_to root_path
        end
      end
    end

    context 'non existing event' do
      let(:bad_event_id) { '20w' }
      subject { get :edit, params: { id: bad_event_id } }

      it 'should raise exception' do
        expect { Event.find(bad_event_id) }.to raise_exception(ActiveRecord::RecordNotFound)
      end

      it 'should respond with a 404' do
        expect(subject).to have_http_status(404)
      end
    end
  end

  describe 'PATCH #update' do
    let(:user) { create(:user, email: 'test@test.com', password: 'P@ssw0rd') }
    let(:event) { create(:event, user: user) }

    context 'with logged in user' do

      context 'owner of the event' do
        before(:each) do
          login_as(user)
        end

        context 'with valid attributes' do
          subject do
            patch :update, params: { id: event.id,
                                     event: attributes_for(:event, description: 'I changed the description and the location of this event',
                                                                   location: 'Test2') }
          end

          it 'should change the event attributes' do
            subject
            event.reload
            expect(event.description).to eq('I changed the description and the location of this event')
            expect(event.location).to eq('Test2')
          end

          it 'should redirect to updated event' do
            subject
            expect(subject).to redirect_to event_path(event.id)
          end
        end

        context 'with invalid attributes' do
          subject do
            patch :update, params: { id: event.id,
                                     event: attributes_for(:event, description: 'short',
                                                                   location: 'Test2') }
          end
          it 'should not change the event attributes' do
            subject
            event.reload
            expect(event.description).to_not eq('short')
            expect(event.location).to_not eq('Test2')
          end
        end
      end

      context 'not owner of the event' do
        let(:user1) { create(:user, email: 'test1@test.com', password: 'P@ssw0rd') }
        subject do
          patch :update, params: { id: event.id,
                                   event: attributes_for(:event, description: 'I changed the description and the location of this event',
                                                         location: 'Test2') }
        end
        before(:each) do
          login_as(user1)
        end

        it 'should not change the event attributes' do
          subject
          event.reload
          expect(event.description).to_not eq('I changed the description and the location of this event')
          expect(event.location).to_not eq('Test2')
        end
      end
    end

    context 'with not logged in user' do
      subject do
        patch :update, params: { id: event.id,
                                 event: attributes_for(:event, description: 'I changed the description and the location of this event',
                                                       location: 'Test2') }
      end
      it 'should not change the event attributes' do
        subject
        event.reload
        expect(event.description).to_not eq('I changed the description and the location of this event')
        expect(event.location).to_not eq('Test2')
      end
    end
  end

  context 'DELETE #destroy' do
    let(:user) { create(:user) }
    let(:event) { create(:event, user: user) }

    context 'existing event' do
      subject { delete :destroy, params: { id: event.id } }

      context 'with logged in user' do
        context 'is owner of event' do
          before(:each) do
            login_as(user)
          end

          it 'should delete event' do
            event
            expect { subject }.to change(Event, :count).by(-1)
          end

          it 'should redirect to root' do
            expect(subject).to redirect_to root_path
          end
        end

        context 'is not owner of event' do
          let(:user1) { create(:user, email: 'test1@test.com') }
          before(:each) do
            login_as(user1)
          end

          it 'should not delete event' do
            subject
            expect { subject }.to_not change(Event, :count)
          end
        end
      end

      context 'with not logged in user' do
        it 'should not delete event' do
          subject
          expect { subject }.to_not change(Event, :count)
        end
      end
    end

    context 'non existing event' do
      let(:bad_event_id) { '20w' }
      subject { delete :destroy, params: { id: bad_event_id } }

      it 'should raise exception' do
        expect { Event.find(bad_event_id) }.to raise_exception(ActiveRecord::RecordNotFound)
      end

      it 'should respond with a 404' do
        expect(subject).to have_http_status(404)
      end
    end
  end
end
