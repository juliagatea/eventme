require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  context 'GET #index' do
    subject { get :index }
    let(:admin) { create(:user, is_admin: true) }

    before(:each) do
      login_as(admin)
    end
    context 'logged in user is admin' do
      it { should have_http_status(200) }
    end

    context 'logged in user is not admin' do
      let(:user) { create(:user) }

      it 'should redirect to root path' do
        login_as(user)
        expect(subject).to redirect_to root_path
      end
    end
  end

  context 'PUT #promote' do
    let(:admin) { create(:user, is_admin: true) }
    subject { put :promote, params: { id: user.id } }

    context 'logged in user is admin' do
      before(:each) do
        login_as(admin)
      end

      context 'user managed is current_user' do
        let(:user) { admin }

        it 'should redirect to admins path' do
          expect(subject).to redirect_to users_path
        end
      end

      context 'user managed is not current_user' do
        context 'user is already admin' do
          let(:user) { create(:user, is_admin: true) }

          it 'should do nothing' do
            subject
            user.reload
            expect(user.is_admin).to be(true)
          end

          it 'should redirect to admins path' do
            expect(subject).to redirect_to users_path
          end
        end

        context 'user is not admin' do
          context 'user is active' do
            let(:user) { create(:user) }

            it 'should promote user to admin' do
              subject
              user.reload
              expect(user.is_admin).to be(true)
            end
          end

          context 'user is inactive' do
            let(:user) { create(:user, is_active: false) }
            it 'should not promote user to admin' do
              subject
              user.reload
              expect(user.is_admin).to be(false)
            end
          end
        end
      end
    end

    context 'logged in user is not admin' do
      let(:user) { create(:user) }

      it 'should redirect to root path' do
        login_as(user)
        expect(subject).to redirect_to root_path
      end
    end
  end

  context 'PUT #switch_activation' do
    let(:admin) { create(:user, is_admin: true) }
    subject { put :switch_activation, params: { id: user.id } }

    context 'logged in user is admin' do
      before(:each) do
        login_as(admin)
      end

      context 'user managed is current_user' do
        let(:user) { admin }

        it 'should redirect to admins path' do
          expect(subject).to redirect_to users_path
        end
      end

      context 'user managed is not current_user' do
        context 'active user' do
          let(:user) { create(:user) }

          it 'should set user inactive' do
            subject
            user.reload
            expect(user.is_active).to_not be(true)
          end

          it 'should redirect to admins path' do
            expect(subject).to redirect_to users_path
          end
        end

        context 'inactive user' do
          let(:user) { create(:user, is_active: false) }

          it 'should set user active' do
            subject
            user.reload
            expect(user.is_active).to_not be(false)
          end

          it 'should redirect to admins path' do
            expect(subject).to redirect_to users_path
          end
        end
      end
    end

    context 'logged in user is not admin' do
      let(:user) { create(:user) }

      it 'should redirect to root path' do
        login_as(user)
        expect(subject).to redirect_to root_path
      end
    end
  end

  context 'DELETE #destroy' do
    subject { delete :destroy, params: { id: user.id } }

    context 'logged in user is admin' do
      let(:admin) { create(:user, is_admin: true) }
      before(:each) do
        login_as(admin)
      end
      context 'user managed is current_user' do
        let(:user) { admin }

        it 'should redirect to admins path' do
          expect(subject).to redirect_to users_path
        end
      end

      context 'user managed is not current_user' do
        let(:user) { create(:user) }

        it 'should delete user' do
          user
          expect { subject }.to change(User, :count).by(-1)
        end

        it 'should redirect to admins path' do
          expect(subject).to redirect_to users_path
        end
      end
    end

    context 'logged in user is not admin' do
      let(:user) { create(:user) }

      it 'should redirect to root path' do
        login_as(user)
        expect(subject).to redirect_to root_path
      end
    end
  end

  context 'GET #events' do
    let(:event) { create(:event, :with_subscribers) }
    let(:subscriber) { event.subscribers.first }
    subject { get :events }

    context 'logged in user' do
      before(:each) do
        login_as(subscriber)
      end

      it 'should have http status success' do
        expect(subject).to have_http_status(200)
      end
    end

    context 'not logged in user' do
      it 'should redirect to root path' do
        expect(subject).to redirect_to root_path
      end
    end
  end
end

