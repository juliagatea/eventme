FactoryBot.define do
  factory :event do
    name { 'Test Event' }
    description { 'This is the description of Test Event ' }
    date { '2019-05-05' }
    location { 'Test Location' }
    association :user
    trait :invalid do
      description { 'TooShort' }
    end
    trait :with_subscribers do
      after(:create) do |event|
        event.subscribers << create(:user, email: 'test1@test.com')
      end
    end
  end
end
