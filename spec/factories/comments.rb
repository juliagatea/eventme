FactoryBot.define do
  factory :comment do
    description { 'This is the first comment' }
    association :user
    association :event
    trait :invalid do
      description { 'TooShort' }
    end
  end
end
