FactoryBot.define do
  sequence :email do |n|
    "User#{n}@test.com"
  end

  factory :user do
    first_name { 'User' }
    last_name { 'Test' }
    email { generate(:email) }
    password { 'User!23' }
  end
end
