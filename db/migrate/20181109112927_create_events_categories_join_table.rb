class CreateEventsCategoriesJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :events, :categories do |t|
      t.index [:event_id, :category_id]
    end
  end
end
