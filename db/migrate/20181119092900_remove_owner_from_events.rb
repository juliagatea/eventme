class RemoveOwnerFromEvents < ActiveRecord::Migration[5.2]
  def change
    change_table :events do |t|
      t.remove_references :owner, :polymorphic => true
    end
    add_reference :events, :user, index: true
  end
end
