# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
users_list = []
(1..10).each do |index|
  users_list << { first_name: "User#{index}", last_name: 'Test', email: "user#{index}@test.com" }
end

users_list.each do |user_attributes|
  new_user = User.find_or_initialize_by(email: user_attributes[:email])
  user_attributes[:password] = 'P@ssw0rd'
  new_user.update_attributes(user_attributes)
end

admin = User.first
admin.update_attributes(is_admin: true)

categories_list = [{ name: 'Theatre' },
                   { name: 'Opera' },
                   { name: 'Dance' },
                   { name: 'Movie' },
                   { name: 'Concert' },
                   { name: 'Conference' },
                   { name: 'Sport' },
                   { name: 'Kids' },
                   { name: 'Exposition' },
                   { name: 'Literature' }]

categories_list.each do |category|
  Category.find_or_create_by(name: category[:name])
end

@users = User.all
events_list = [{ name: 'Pinocchio', description: 'Pinocchio is known for having a short nose that becomes longer when he is under stress, especially while lying.', date: '2018-09-13', location: 'Alba', user: @users[0], category: 'Kids' },
               { name: 'Madame Butterfly', description: 'US-American Lieutenant Pinkerton marries Cio-Cio-San, but only for a short time; a commonly accepted convention at this time. Whilst for him it was but a fleeting affair, for her, it was true love. After his departure, she waits longingly for his return. When he finally reappears, Cio-Cio-San – the mother of his child – realises that he has remarried. Out of utter desperation, she kills herself', date: '2018-10-8', location: 'Sibiu', user: @users[1], category: 'Theatre' },
               { name: 'Electric Castle', description: 'Electric Castle is a Romanian music festival that takes place every year on the Transylvanian spectacular domain of Banffy Castle, near Cluj-Napoca.', date: '2019-07-07', location: 'Bontida', user: @users[2], category: 'Concert' },
               { name: 'Christmas Concert by Stefan Hrusca', description: 'It’s not Christmas until Stefan Hrusca carols. Stefan Hrusca is a Romanian folk singer known for his Christmas carols. He was born in Ieud, Maramureș County', date: '2018-12-20', location: 'Cluj', user: @users[3], category: 'Concert' },
               { name: 'Contemporary dance', description: 'Contemporary dance is a style of expressive dance that combines elements of several dance genres including modern, jazz, lyrical and classical ballet. Contemporary dancers strive to connect the mind and the body through fluid dance movements.', date: '2019-01-10', location: 'Cluj', user: @users[4], category: 'Dance' },
               { name: 'Real Madrid Legends vs Arsenal Legends', description: 'Legends from two great clubs put on a show for the Corazon Classic Match 2018 - with Real Madrid just about coming out on top. Luis Boa Morte scored for us but his neat finish was sandwiched by goals from Raul and Guti, and the home side held on despite some late pressure from our former stars.', date: '2018-03-05', location: 'Bucuresti', user: @users[5], category: 'Sport' },
               { name: 'Ed Sheeran', description: 'Edward Christopher Sheeran, MBE is an English singer, songwriter, guitarist, record producer, and actor. Sheeran was born in Halifax, West Yorkshire.', date: '2019-09-20', location: 'Bucuresti', user: @users[6], category: 'Concert' },
               { name: 'Mircea Cartarescu - Solenoid', description: 'Mircea Cartarescu (born 1 June 1956) is a Romanian poet, novelist, literary critic and essayist.', date: '2018-11-13', location: 'Aiud', user: @users[7], category: 'Literature'},
               { name: 'U NTT Data - CS Alba BLaj', description: 'The third stage of the A1 Women Division will take place on Saturday, October 10th, and the U NTT Data Cluj will play with CSM Volleyball Alba-Blaj. The match will be held from 18:00 and will be arbitrated by Marius and Bogdan, the delegated observer being Silvian Lungu.', date: '2018-11-14', location: 'Blaj', user: @users[8], category: 'Sport' },
               { name: 'Bohemian Rhapsody', description: 'Bohemian Rhapsody is a foot-stomping celebration of Queen, their music and their extraordinary lead singer Freddie Mercury. Freddie defied stereotypes and shattered convention to become one of the most beloved entertainers on the planet.', date: '2018-10-15', location: 'Cluj', user: @users[9], category: 'Movie' },
               { name: 'Kings on ice', description: 'După cinci ediții Kings On Ice desfășurate la București cu casa inchisă, unul dintre cele mai iubite sporturi din lume, patinajul artistic, vine în 2015 și la Cluj', date: '2018-12-20', location: 'Cluj', user: admin, category: 'Dance' },
               { name: 'Untold', description: 'Untold Festival is the largest annual electronic music festival held in Romania, taking place in Cluj-Napoca at Central Park, with the main stage at Cluj Arena.', date: '2019-08-01', location: 'Cluj', user: admin, category: 'Concert' }]

events_list.each do |event_attributes|
  new_event = Event.find_or_initialize_by(name: event_attributes[:name])
  new_event.update_attributes(event_attributes.except(:category))
  category = Category.find_by_name(event_attributes[:category])
  new_event.categories << category unless new_event.categories.find_by(id: category.id)
  subscriber_id = (User.ids - [event_attributes[:user].id]).sample
  new_event.subscribers << User.find(subscriber_id) unless new_event.subscribers.find_by(id: subscriber_id)
end
