Rails.application.routes.draw do
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :registrations, only: %i[new create], path: 'sign_up', path_names: { new: '' }
  resources :events do
    resource :subscribers, only: %i[create destroy]
    resources :comments
  end
  resources :users, only: %i[index destroy] do
    put :promote, on: :member
    put :switch_activation, on: :member
  end
  resource :users, only: [] do
    get :events
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'events#index'
end
