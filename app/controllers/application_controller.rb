class ApplicationController < ActionController::Base
  include SessionsHelper
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def require_user
    return true if current_user

    flash[:danger] = 'You must be logged in to perform that action'
    redirect_to root_path
  end

  def require_owner(object)
    return true if current_user.id == object.user_id

    flash[:danger] = 'You must be the owner of the event to perform that action'
    redirect_to root_path
  end

  def record_not_found
    render template: 'shared/record_not_found', status: 404
  end
end
