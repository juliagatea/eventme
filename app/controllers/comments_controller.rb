class CommentsController < ApplicationController
  before_action :require_user
  before_action :set_event
  before_action :set_comment, except: [:create]
  before_action -> { require_owner(@comment) }, only: %i[update destroy]

  def create
    @comment = Comment.new(comment_params)
    if @comment.save
      flash[:success] = 'Comment was successfully created.'
    else
      flash[:danger] = 'Error creating comment.'
    end
    redirect_to event_path(@event)
  end

  def update
    if @comment.update(comment_params)
      flash[:success] = 'Comment was successfully updated.'
    else
      flash[:danger] = 'Error updating comment.'
    end
    redirect_to event_path(@event)
  end

  def destroy
    @comment.destroy
    flash[:success] = 'Comment was successfully deleted.'
    redirect_to event_path(@event)
  end

  def new
    @comment = Comment.new
  end

  def edit
    respond_to do |format|
      format.js { render layout: false }
    end
  end
  
  private

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def set_event
    @event = Event.find(params[:event_id])
  end

  def comment_params
    params.fetch(:comment, {}).permit(:description).tap do |params|
      params[:user] = current_user
      params[:event] = @event
    end
  end
end
