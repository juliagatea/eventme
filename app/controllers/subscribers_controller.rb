class SubscribersController < ApplicationController
  before_action :set_event
  before_action :require_user
  before_action :check_owner

  def create
    if current_user.subscribed_to?(@event)
      flash[:warning] = 'You have already subscribed to this event'
    else
      @event.subscribers << current_user
      flash[:success] = 'You have subscribed to this event'
      redirect_to event_path(@event)
    end
  end

  def destroy
    if current_user.subscribed_to?(@event)
      @event.subscribers.destroy(current_user)
      flash[:success] = 'You have successfully unsubscribed from this event'
      redirect_to event_path(@event)
    else
      flash[:warning] = 'You have already unsubscribed from this event'
    end
  end

  private

  def set_event
    @event = Event.find(params[:event_id])
  end

  def check_owner
    return true unless current_user == @event.user

    flash[:danger] = 'You are not allowed to do this action'
    redirect_to event_path(@event)
  end
end
