class EventsController < ApplicationController
  before_action :set_event, except: %i[index new create]
  before_action :set_categories, only: %i[new edit]
  before_action :require_user, except: %i[index show]
  before_action -> { require_owner(@event) }, only: %i[edit update destroy]

  def index
    @events = Event.order(:date)
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      flash[:success] = 'Event was successfully created.'
      redirect_to event_path(@event)
    else
      render :new
    end
  end

  def show
    @comment = Comment.new(event_id: @event.id)
    @comments = @event.comments
  end

  def edit
  end

  def update
    if @event.update(event_params)
      flash[:success] = 'Event was successfully updated.'
      redirect_to event_path(@event)
    else
      render :edit
    end
  end

  def destroy
    @event.destroy
    redirect_to root_path
  end

  private

  def set_event
    @event = Event.find(params[:id])
  end

  def set_categories
    @categories = Category.all
  end

  def event_params
    params.fetch(:event, {}).permit(:name, :description, :date, :location, category_ids: []).tap do |params|
      params[:user] = current_user
    end
  end
end
