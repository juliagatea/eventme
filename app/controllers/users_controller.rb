class UsersController < ApplicationController
  before_action :require_user, only: [:events]
  before_action :require_admin, except: [:events]
  before_action :set_user, except: %i[index events]
  before_action :check_if_user_is_current_user, except: %i[index events]

  def index
    @users = User.where.not(id: current_user).order(:email)
  end

  def promote
    @user.is_admin = true
    if @user.valid?
      @user.save
    else
      flash[:warning] = 'User is inactive. You are not allowed to do this action'
    end
    redirect_to users_path
  end

  def switch_activation
    @user.toggle(:is_active).save
    redirect_to users_path
  end

  def destroy
    @user.destroy
    redirect_to users_path
  end

  def events
    @events = current_user.events
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def require_admin
    return true if logged_in? && current_user.is_admin

    flash[:danger] = 'You are not allowed to do this action'
    redirect_to root_path
  end

  def check_if_user_is_current_user
    return true unless @user == current_user

    flash[:danger] = 'You are not allowed to do this action'
    redirect_to users_path
  end
end
