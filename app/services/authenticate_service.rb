module AuthenticateService
  class << self
    def authenticate(user, password)
      user && decrypt(user.password) == password
    end

    def encryptor
      @encryptor ||= ActiveSupport::MessageEncryptor.new(ENV['KEY'])
    end

    def decrypt(password)
      encryptor.decrypt_and_verify(password)
    end
  end
end
