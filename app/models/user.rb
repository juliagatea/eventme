class User < ApplicationRecord
  include Validations
  include Encryption
  validates_presence_of :first_name, :last_name
  validates_length_of :first_name, :last_name, minimum: 3, maximum: 25
  validate :user_active, if: proc { |user| user.changes.include?('is_admin') }
  before_save :downcase_email
  has_many :events
  has_and_belongs_to_many :events
  has_many :comments

  def full_name
    "#{first_name} #{last_name}"
  end

  def subscribed_to?(event)
    events.include? event
  end

  def owner_of?(event)
    event.user == self
  end
  
  private

  def downcase_email
    self.email = email.downcase
  end

  def user_active
    return if is_active

    errors[:base] << 'User is inactive.'
  end

end
