class Comment < ApplicationRecord
  validates_presence_of :description
  validates_length_of :description, minimum: 10
  belongs_to :user
  belongs_to :event
end
