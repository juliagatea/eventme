class Event < ApplicationRecord
  validates_presence_of :name, :description, :date, :location
  validates_length_of :name, minimum: 3, maximum: 25
  validates_length_of :description, minimum: 15
  belongs_to :user
  has_and_belongs_to_many :subscribers, class_name: 'User'
  has_and_belongs_to_many :categories
  has_many :comments
end
